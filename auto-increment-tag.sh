#!/bin/sh

# Check if an argument is provided
if [ $# -eq 0 ]; then
    echo "Usage: $0 <string>"
    exit 1
fi

# img_path="https://registry.hub.docker.com/v2/repositories/eonintelligence/kaloriekompass-apis/tags/"

# Read the string argument and store it in a variable
img_path="$1"

# Use wget to retrieve the JSON data
json_data=$(wget -qO- "$img_path")

# Define the expected JSON string
expected_json='{"count":0,"next":null,"previous":null,"results":[]}'

# Check if the JSON data matches the expected string
if [ "$json_data" = "$expected_json" ]; then
    tag="1.0"
else
    # Alternative 
    tag=$(wget -qO- $img_path | grep -o '"name":"[^"]*' | awk -F ':"' '{print $2}')
    # tag=$(curl -s $img_path | grep -o '"name":"[^"]*' | awk -F ':"' '{print $2}')
fi

#Replace the dots with spaces
tag_no_dots=$(echo $tag | tr '.' ' ')

set -- $tag_no_dots

# Calculate the incremented second part (assuming a format like "X.Y")
second_part=$(( $2 + 1 ))

# Construct the new version string
new_version="$1.$second_part"

echo "$new_version"

# --------------------------------------
# Split the tag into an array
# tag_parts=$($tag_no_dots)

# Calculate the incremented second part (assuming a format like "X.Y.Z")
# second_part=$((${tag_parts[1]} + 1))

# Construct the new version string
# new_version="${tag_parts[0]}.$second_part"

# Convert the new version to a float
# float_version=$(bc -l <<< "$new_version")

# echo $float_version